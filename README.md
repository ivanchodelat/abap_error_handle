# SAP Abap error_handle
Son algunas funciones escritas en ABAP, para las tareas de manejo de errores, deubug y log.
### Objetivo
Busco tener una serie de excepciones y clases que hagan el manejo de errores mas eficiente, para que el código sea más re utilizable. 

![](https://www.filepicker.io/api/file/LVssKILlSH6K5N2zukHf)


##O bjetos desarrollo
|Tipo|Nombre|
|Paquete|Z_ITS_MOBILE_CO_FRAMEWORK|
|Trasporte Ides 2020||


## Clase mensajes_bapi

Permite que  dar manejo a los mensajes de una bapi. Almaceneandolos y mostrandolos cuando es requerido.


|Macro|Función|
|--------|-----------|
|_bapi_debug_log| Las funciones de debug + guardar el resultado de return|
|_bapi_debug_log_mensaje| Igual que _bapi_debug_log + si hay mensajes de error los muesra.|

|Método|Función|
|------|-------|
|contiene_errores| retorna true si hay mensajes de error|
|mostrar_errores|Muestra los mensajes de error|
|agregar_mensajes|se agregan losmensajes que esten en it_return_in[]|

Imagenes:


![](https://www.filepicker.io/api/file/ITySz90dSy5uFnE5OFHC)

![](https://www.filepicker.io/api/file/uWz7EKZ1Q5uOuSkDuHn6)

## Clase Debug
Esta clase busca llevar un log de todo lo que va haciendo un desarrollo en cuanto a datos, para usar en el momento en que se construye el desarrollo y para el mantenimiento posterior.
Se busca que cada instrucción sea muy “corta” para  crear el mínimo ruido posible en el código de negocios.
Cuenta con los siguientes Macros que se “insertan” entre el código. 
 Mediante una variable en la transacción STVARV se activa o desactiva el modo debug.

|Macro|Función|
|--------|-----------|
|debug_inicio 'Modo debug Activado'.|Aquí comienza el Log|
| debug_metodo_actual.|dentro de un método o formulario imprime en el log el nombre del método y quien lo llamo|
| debug_stack.|Muestra las 5 ultimas entradas de la pila ABAP|
|  debug_texto  'Texto..'.|Imprime el texto, para comentarios|
|  debug_datos  it_itab 'it_itab'."#EC NO_TEXT|Muestra una variable, estructura o tabla y el texto|
| debug_titulo_seccion 'Nueva Sección'."#EC NO_TEXT|Imprime en formato titulo|
|  debug_nombres_campos 'ZSPP_002_RESB'.|Muestra los campos y descripción de estos de una tabla o estructura del diccionario ABAP.|

**Nota personal:** Aunque la idea no me parecía del todo “limpia” he comenzado a ahorrar  tiempo en búsquedas de tablas y nombres de campos, seguiré desarrollándola. 


### Algunas pantallas.

![](https://www.filepicker.io/api/file/WecaeXJrQxKTFj8aX3vA)
![](https://www.filepicker.io/api/file/jWh7bNQNTZ67dRUtkXbV)

### Guardando el LOG.
Es posible guardar el log de lo que visualiza en la ventana SAP mediante la siguiente Manera:    

- En la ventana del metodo debug, selecionar todo y copiar CTRL + C.

- En el programa NotePad++ selecionar: Edit,Paste Special, Paste HTML Content.

- Luego grabar el archivo con extension HTML
- Abrir desde un Navegador como Google Crome.
![](https://www.filepicker.io/api/file/mPBKDTZgT5ahrsLfCE9a)


## Almacenando el Log debug.

|objeto|Nombre|Descripción|
|
|Tabla|String del debug Log|Tabla que guarda el log de aplicaciones|
