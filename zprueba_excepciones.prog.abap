REPORT zprueba_excepciones.
INCLUDE zprueba_excep_sap_calls_class.
INCLUDE zprueba_excepciones_lcl_modelo.
DATA ob_modelo  TYPE REF TO lcl_modelo.

*..Objetos Excepciones
DATA r_cx_sql TYPE REF TO cx_sy_open_sql_db.
DATA r_cx_root TYPE REF TO cx_root.
DATA r_zcx_call TYPE REF TO zcx_call_function.

DATA time  TYPE timezone.

START-OF-SELECTION.
  CREATE OBJECT ob_modelo.
  TRY.
      time = calls_class=>func_get_system_timezone( ).

      ob_modelo->grabar( ).
      WRITE: / 'Ninguna Excepcion capturada'.
    CATCH zcx_call_function into r_zcx_call.
        write: / r_zcx_call->get_text( ).
        WRITE: / 'función que fallo ', r_zcx_call->function_name.
        WRITE: / 'SY-SUBRC ',r_zcx_call->sy_subrc.
    CATCH cx_sy_open_sql_db INTO r_cx_sql.
      MESSAGE e398(00) WITH r_cx_sql->get_text( ).
*.. algo inesperado paso
    CATCH cx_root INTO DATA(gcx_root).
      cl_demo_output=>end_section( ).
      cl_demo_output=>begin_section( |-------ERROR-------| ).
      IF gcx_root->previous IS NOT INITIAL.
        cl_demo_output=>write_text(  gcx_root->previous->get_text( ) ).
      ENDIF.
      cl_demo_output=>write_text(  gcx_root->get_text( ) ).
      gcx_root->get_source_position(
        IMPORTING
          program_name = DATA(program_name)
          include_name = DATA(include_name)
          source_line  = DATA(source_line)
      ).
      cl_demo_output=>write_text( |program_name { program_name }| ).
      cl_demo_output=>write_text( |include_name { include_name }| ).
      cl_demo_output=>write_text( |source_line  { source_line }| ).
      cl_demo_output=>display( ).
  ENDTRY.
  

  *..Ahotirizacion fallida
  CATCH cx_authorization_missing INTO DATA(cx_aut).
    MESSAGE 'Usted no tiene autorización.'(001)  TYPE 'E'.
*.. No hay datos pra los parametros selecionados
  CATCH cx_list_error_empty_list INTO DATA(cx_list).
    MESSAGE TEXT-005 TYPE  'I' DISPLAY LIKE 'E'.
