* Include zprueba_excep_sap_calls_class

CLASS calls_class DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS func_get_system_timezone
                RETURNING VALUE(r_timezone) TYPE timezone
                RAISING zcx_call_function.
  PROTECTED SECTION.
    CLASS-DATA subrc_buffer TYPE sy-subrc.
ENDCLASS.

CLASS calls_class IMPLEMENTATION.




  METHOD func_get_system_timezone.
* EXPORTING VALUE(r_timezone) TYPE timezone.
    CALL FUNCTION 'GET_SYSTEM_TIMEZONE'  "GET_SYSTEM_TIMEZONE
      IMPORTING
        timezone            = r_timezone
      EXCEPTIONS
        customizing_missing = 1
        OTHERS              = 2.
    IF sy-subrc <> 0.
      subrc_buffer = sy-subrc.
      RAISE EXCEPTION TYPE zcx_call_function
        EXPORTING
          function_name = 'GET_SYSTEM_TIMEZONE'   "GET_SYSTEM_TIMEZONE
          sy_subrc      = subrc_buffer.
    ENDIF.
  ENDMETHOD.

ENDCLASS.
