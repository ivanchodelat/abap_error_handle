CLASS zmensajes_bapi DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .
  PUBLIC SECTION.
    METHODS constructor.
    METHODS contiene_errores RETURNING VALUE(r_return) TYPE boolean.
    METHODS in_contiene_errores RETURNING VALUE(r_return) TYPE boolean.
    METHODS mostrar_errores_in.
    METHODS mostrar_errores.
    METHODS get_errores.
    METHODS mostrar_log_nivel IMPORTING i_nivel TYPE i.
    METHODS get_log_nivel IMPORTING i_nivel TYPE i.
    METHODS agregar_mensajes.
    METHODS borrar_todo.
    METHODS agregar_mensajes_bapireturn.
    METHODS alv_nivel
      IMPORTING
        i_nivel TYPE i RAISING cx_salv_msg.

    DATA it_return_in TYPE STANDARD TABLE OF bapiret2.
    DATA it_return_out TYPE STANDARD TABLE OF bapiret2 READ-ONLY.
    DATA it_bapireturn TYPE STANDARD TABLE OF bapireturn.
  PRIVATE SECTION.
* types for messages
    DATA wa_mensajes_in TYPE  bapireturn.
    TYPES: BEGIN OF esp1_message_wa_type,
             msgid  LIKE sy-msgid,
             msgty  LIKE sy-msgty,
             msgno  LIKE sy-msgno,
             msgv1  LIKE sy-msgv1,
             msgv2  LIKE sy-msgv2,
             msgv3  LIKE sy-msgv3,
             msgv4  LIKE sy-msgv4,
             lineno TYPE mesg-zeile,
           END OF esp1_message_wa_type.
    TYPES: esp1_message_tab_type TYPE esp1_message_wa_type.
    DATA it_mensajes TYPE STANDARD TABLE OF   esp1_message_tab_type.
    DATA wa_mensajes  TYPE    esp1_message_wa_type.
    DATA it_return TYPE STANDARD TABLE OF bapiret2.
    METHODS get_nivel_2.
    METHODS mostrar_it_return_out.
    METHODS it_return_to_it_mensajes.
    METHODS get_nivel_3.
ENDCLASS.

CLASS zmensajes_bapi IMPLEMENTATION.
  METHOD constructor.
    me->borrar_todo( ).
  ENDMETHOD.

  METHOD contiene_errores.
    me->get_errores( ).
    IF it_return_out IS NOT INITIAL.
      r_return = abap_true.
    ENDIF.
  ENDMETHOD.

  METHOD in_contiene_errores.
    TYPES  lv_type_range      TYPE  RANGE OF bapiret2-type.      "
    DATA(lv_type_range) = VALUE lv_type_range(  ( sign = 'I' option = 'CP' low = 'E' ) ).
    CLEAR it_return_out.
    it_return_out[] = it_return_in[].
    DELETE it_return_out WHERE type NOT IN lv_type_range.
    IF it_return_out IS NOT INITIAL.
      r_return = abap_true.
    ENDIF.
  ENDMETHOD.

  METHOD get_errores.
    TYPES  lv_type_range      TYPE  RANGE OF bapiret2-type.      "
    DATA(lv_type_range) = VALUE lv_type_range(  ( sign = 'I' option = 'CP' low = 'E' ) ).
    it_return_out[] = it_return[].
    DELETE it_return_out WHERE type NOT IN lv_type_range.
  ENDMETHOD.

  METHOD get_log_nivel.
    CASE i_nivel.
      WHEN 1."Errores
        me->get_errores( ).
      WHEN 2."Errores + Exitos
        me->get_nivel_2( ).
      WHEN 3."Errores + Exitos + Info
        me->get_nivel_3( ).
      WHEN 4."Errores + Exitos + Info + Warnings.
        it_return_out[] = it_return[].
      WHEN OTHERS.
    ENDCASE.
  ENDMETHOD.

  METHOD get_nivel_2.
    TYPES  lv_type_range      TYPE  RANGE OF bapiret2-type.      "
    DATA(lv_type_range) = VALUE lv_type_range(  ( sign = 'I' option = 'CP' low = 'E' )
                                                ( sign = 'I' option = 'CP' low = 'S' ) ).
    it_return_out[] =  it_return[].
    DELETE it_return_out WHERE type NOT IN lv_type_range.
  ENDMETHOD.


  METHOD get_nivel_3.
    TYPES  lv_type_range      TYPE  RANGE OF bapiret2-type.      "
    DATA(lv_type_range) = VALUE lv_type_range(  ( sign = 'I' option = 'CP' low = 'E' )
                                                ( sign = 'I' option = 'CP' low = 'S' )
                                                ( sign = 'I' option = 'CP' low = 'I' ) ).
    it_return_out[] =  it_return[].
    DELETE it_return_out WHERE type NOT IN lv_type_range.
  ENDMETHOD.

  METHOD mostrar_errores_in.
    me->mostrar_it_return_out( ).
  ENDMETHOD.
  METHOD mostrar_errores.
    me->get_errores( ).
    me->mostrar_it_return_out( ).
  ENDMETHOD.


  METHOD mostrar_it_return_out.
    me->it_return_to_it_mensajes( ).
    CALL FUNCTION 'C14Z_MESSAGES_SHOW_AS_POPUP'
      TABLES
        i_message_tab = it_mensajes.
  ENDMETHOD.


  METHOD mostrar_log_nivel.
    me->get_log_nivel( i_nivel ).
    me->mostrar_it_return_out( ).
  ENDMETHOD.



  METHOD agregar_mensajes.
    LOOP AT it_return_in INTO DATA(wa_return).
      APPEND wa_return TO it_return.
    ENDLOOP.
  ENDMETHOD.


  METHOD it_return_to_it_mensajes.
    CLEAR it_mensajes.
    LOOP AT it_return_out REFERENCE INTO DATA(ref_return).
      wa_mensajes-msgid = ref_return->id.
      wa_mensajes-msgty = ref_return->type.
      wa_mensajes-msgno = ref_return->number.
      wa_mensajes-msgv1 = ref_return->message_v1.
      wa_mensajes-msgv2 = ref_return->message_v2.
      wa_mensajes-msgv3 = ref_return->message_v3.
      wa_mensajes-msgv4 = ref_return->message_v4.
      wa_mensajes-lineno = ref_return->row.
      APPEND wa_mensajes TO it_mensajes.
    ENDLOOP.

  ENDMETHOD.

  METHOD agregar_mensajes_bapireturn.
    DATA wa_return TYPE bapiret2.
    CLEAR it_return_in.
    LOOP AT it_bapireturn INTO DATA(wa_bapireturn).
      CALL FUNCTION 'BALW_RETURN_TO_RET2'
        EXPORTING
          return_in = wa_bapireturn
        IMPORTING
          return_ou = wa_return.
      APPEND wa_return TO it_return.
      APPEND wa_return TO it_return_in.
    ENDLOOP.
  ENDMETHOD.

  METHOD borrar_todo.
    CLEAR me->it_return_in.
    CLEAR me->it_return_out.
    CLEAR me->it_return.
  ENDMETHOD.



  METHOD alv_nivel.
    obj_msjs->get_log_nivel(  i_nivel ).
*..Alv
    DATA obj_salv_table TYPE REF TO cl_salv_table.
    DATA lt_mensajes LIKE obj_msjs->it_return_out.
    lt_mensajes[] = obj_msjs->it_return_out[].
    DELETE ADJACENT DUPLICATES FROM lt_mensajes COMPARING ALL FIELDS.
    cl_salv_table=>factory( IMPORTING  r_salv_table = obj_salv_table
                            CHANGING   t_table =  lt_mensajes ).
    DATA(ref_col) = obj_salv_table->get_columns( ).
    ref_col->set_optimize(  ).
    obj_salv_table->display( ).
  ENDMETHOD.

    ENDCLASS.