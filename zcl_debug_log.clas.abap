class ZCL_DEBUG_LOG definition
  public
  final
  create public .
  PUBLIC SECTION.
    CLASS-DATA: atrv_text TYPE string.
    CLASS-DATA: atrv_t_abap_stack TYPE  abap_callstack.
    CLASS-DATA nombre_pramero TYPE string READ-ONLY.
    CLASS-DATA: usuario TYPE string READ-ONLY.
    CLASS-DATA: debug_mostrar TYPE boolean READ-ONLY.  "Bandera si debug y si debe mostrar.
*..Metodos
    CLASS-METHODS debug_forzado IMPORTING VALUE(i_text) TYPE string.
    CLASS-METHODS debug_inicio IMPORTING VALUE(i_text) TYPE string.
    CLASS-METHODS is_debug_on IMPORTING VALUE(i_repid) TYPE sy-repid.
    CLASS-METHODS debug_mostrar_data
      IMPORTING
        i_data TYPE data
        i_name TYPE string OPTIONAL  .
    CLASS-METHODS debug_nombres_campos
      IMPORTING
        i_tabla  TYPE string
        i_nombre TYPE string.
    CLASS-METHODS debug_fin.
    CLASS-METHODS debug_mosrtar_texto.
    CLASS-METHODS debug_titulo.
    CLASS-METHODS debug_seccion.
    CLASS-METHODS debug_stack.
    CLASS-METHODS print_metodo_actual.
    CLASS-METHODS fecha_en_texto
      IMPORTING
                VALUE(i_fecha)      TYPE sy-datum
      RETURNING VALUE(r_feha_texto) TYPE string.
    CLASS-METHODS debug_off.
  PRIVATE SECTION.
    CLASS-DATA: debug_users TYPE string.    "Los nombres de los usuarios que se debe mostrar el reporte

    TYPES: BEGIN OF ty_nombres_tablas,
             fieldname TYPE fieldname,
             scrtext_l TYPE scrtext_l,
           END OF ty_nombres_tablas.

    CLASS-DATA it_nombres_tablas TYPE STANDARD TABLE OF ty_nombres_tablas.
    CLASS-DATA wa_nombres_tablas TYPE ty_nombres_tablas.
ENDCLASS.



CLASS ZCL_DEBUG_LOG IMPLEMENTATION.


  METHOD debug_fin.
    IF debug_mostrar EQ abap_true.
      cl_demo_output=>display( ).
    ENDIF.
  ENDMETHOD.


  METHOD debug_forzado.
    "No requiere parametro, siempre activa el Debug
    zcl_debug_log=>debug_mostrar = abap_true.
    zcl_debug_log=>debug_inicio( i_text ).
  ENDMETHOD.


  METHOD debug_inicio.
    zcl_debug_log=>atrv_text = i_text .
    zcl_debug_log=>debug_titulo( ).
    zcl_debug_log=>atrv_text = | Parametro: { zcl_debug_log=>nombre_pramero }|.
    zcl_debug_log=>debug_mosrtar_texto( ).
    zcl_debug_log=>atrv_text = | Usuario: { zcl_debug_log=>usuario }|.
    zcl_debug_log=>debug_mosrtar_texto( ).
    zcl_debug_log=>atrv_text = zcl_debug_log=>fecha_en_texto(  sy-datum ).
    zcl_debug_log=>debug_mosrtar_texto( ).
    DATA lv_hora TYPE char8.
    WRITE: sy-uzeit  TO lv_hora.
    zcl_debug_log=>atrv_text = 'Hora: ' && lv_hora .
    zcl_debug_log=>debug_mosrtar_texto( ).
  ENDMETHOD.


  METHOD debug_mosrtar_texto.
    IF debug_mostrar EQ abap_true.
      cl_demo_output=>write_text( atrv_text ).
    ENDIF.
  ENDMETHOD.


  METHOD debug_mostrar_data.
    IF debug_mostrar EQ  abap_true.
      IF i_name IS NOT INITIAL.
        cl_demo_output=>write_data(
          EXPORTING
            value = i_data
            name  = i_name
        ).
      ELSE.
        cl_demo_output=>write_data(
          EXPORTING
            value = i_data      ).
      ENDIF.
    ENDIF.
  ENDMETHOD.


  METHOD debug_nombres_campos.
    DATA lt_dfies_tab TYPE STANDARD TABLE OF dfies.


    IF debug_mostrar EQ abap_true.
*..Llamar el metodo
      DATA lv_ddobjname TYPE ddobjname.
      lv_ddobjname = i_tabla.
      CLEAR zcl_debug_log=>it_nombres_tablas.
      CLEAR lt_dfies_tab.
      TRY.
          CALL FUNCTION 'DDIF_FIELDINFO_GET'
            EXPORTING
              tabname        = lv_ddobjname
*             fieldname      = ' '
              langu          = sy-langu
*             lfieldname     = ' '
              all_types      = 'X'
*             group_names    = ' '
*             uclen          =
*             do_not_write   = ' '
*            IMPORTING
*             x030l_wa       =
*             ddobjtype      =
*             dfies_wa       =
*             lines_descr    =
            TABLES
              dfies_tab      = lt_dfies_tab
*             fixed_values   =
            EXCEPTIONS
              not_found      = 1
              internal_error = 2
              OTHERS         = 3.
          IF sy-subrc EQ 0.
            LOOP AT lt_dfies_tab REFERENCE INTO DATA(ref_dfies_tab).
              zcl_debug_log=>wa_nombres_tablas-fieldname = ref_dfies_tab->fieldname.
              zcl_debug_log=>wa_nombres_tablas-scrtext_l = ref_dfies_tab->scrtext_l.
              APPEND zcl_debug_log=>wa_nombres_tablas TO  zcl_debug_log=>it_nombres_tablas.
            ENDLOOP.
            zcl_debug_log=>debug_mostrar_data(
              EXPORTING
                i_data = zcl_debug_log=>it_nombres_tablas
                i_name = i_nombre
            ).
          ELSE.
            zcl_debug_log=>atrv_text = 'No fue posible encontrar nombres de campos para:'. "#EC NO_TEXT
            zcl_debug_log=>debug_mosrtar_texto( ).
            zcl_debug_log=>atrv_text = i_tabla.
            zcl_debug_log=>debug_mosrtar_texto( ).
          ENDIF.
        CATCH cx_root INTO DATA(gcx_root).
          zcl_debug_log=>atrv_text = 'No fue posible encontrar nombres de campos para:'. "#EC NO_TEXT
          zcl_debug_log=>debug_mosrtar_texto( ).
          zcl_debug_log=>atrv_text = i_tabla.
          zcl_debug_log=>debug_mosrtar_texto( ).
      ENDTRY.
    ENDIF.
  ENDMETHOD.


  METHOD debug_off.
    debug_mostrar   = abap_false.
  ENDMETHOD.


  METHOD debug_seccion.
    IF debug_mostrar EQ abap_true.
      cl_demo_output=>next_section( atrv_text ).
    ENDIF.
  ENDMETHOD.


  METHOD debug_stack.
    DATA: t_abap_stack TYPE  abap_callstack,
          t_sys_stack  TYPE  sys_callst.

    CLEAR zcl_debug_log=>atrv_t_abap_stack.
    " FM To get the Call Stack information when running the program
    " ABAP Callstack would give you the Program names
    " SYS callstack would give you system programs in callstack
*
    CALL FUNCTION 'SYSTEM_CALLSTACK'
      EXPORTING
        max_level    = 6
      IMPORTING
        callstack    = t_abap_stack
        et_callstack = t_sys_stack.
*
    READ TABLE t_abap_stack TRANSPORTING NO FIELDS
      WITH KEY mainprogram = sy-repid.   "Your Program name
    IF sy-subrc NE 0.

      EXIT.
    ELSE.
      DELETE t_abap_stack INDEX 1.
      zcl_debug_log=>atrv_t_abap_stack = t_abap_stack.
    ENDIF.
  ENDMETHOD.


  METHOD debug_titulo.
    IF debug_mostrar EQ abap_true.
      cl_demo_output=>begin_section( atrv_text ).
    ENDIF.
  ENDMETHOD.


  METHOD fecha_en_texto.
*  IMPORTING
*    value(i_fecha) TYPE sy-datum
*    RETURNING VALUE(r_feha_texto) TYPE string.
    DATA: lt_month TYPE STANDARD TABLE OF t247.
    CALL FUNCTION 'MONTH_NAMES_GET'
      EXPORTING
        language    = sy-langu
      TABLES
        month_names = lt_month.

    READ TABLE lt_month INTO DATA(wa_month)
           WITH KEY spras = sy-langu
           mnr = i_fecha+4(2).
    CONCATENATE i_fecha+6(2) 'de' wa_month-ltx 'de' i_fecha(4)
               INTO r_feha_texto SEPARATED BY space.
  ENDMETHOD.


  METHOD is_debug_on.
    debug_mostrar = abap_false.
    DATA lv_mensaje TYPE string.
    DATA longitud TYPE i.
    nombre_pramero = i_repid && '_DBG_USRS'.
    longitud = strlen( nombre_pramero ).
    IF longitud > 31.
      nombre_pramero = nombre_pramero+0(31). "Solo Caben 31 caracteres en la tabla
    ENDIF.
    usuario = sy-uname.
    SELECT SINGLE low FROM tvarvc
                INTO debug_users
                WHERE name = nombre_pramero
                AND type = 'P'.
    IF sy-subrc EQ 0.
      SEARCH debug_users FOR usuario.
      IF sy-subrc EQ 0.
        debug_mostrar = abap_true.
      ENDIF.
    ENDIF.
  ENDMETHOD.


  METHOD print_metodo_actual.
    DATA:
      bloque_tipo0   TYPE string,
      bloque_tipo1   TYPE string,
      bloque_nombre0 TYPE string,
      bloque_nombre1 TYPE string.
    zcl_debug_log=>debug_stack( ).
    TRY.
        bloque_tipo0 = zcl_debug_log=>atrv_t_abap_stack[ 2 ]-blocktype.
        IF bloque_tipo0 = 'METHOD'.
          bloque_tipo0 = '( )'.
        ELSE.
          bloque_tipo0 = ' '.
        ENDIF.
        bloque_nombre0 = zcl_debug_log=>atrv_t_abap_stack[ 2 ]-blockname.
      CATCH cx_sy_itab_line_not_found.
        bloque_tipo0 = 'Nombre No encontrado'.             "#EC NO_TEXT
    ENDTRY.
    TRY.
        bloque_tipo1 = zcl_debug_log=>atrv_t_abap_stack[ 3 ]-blocktype.
        IF bloque_tipo1 = 'METHOD'.
          bloque_tipo1 = '( )'.
        ELSE.
          bloque_tipo1 = ' '.
        ENDIF.
        bloque_nombre1 = zcl_debug_log=>atrv_t_abap_stack[ 3 ]-blockname.
      CATCH cx_sy_itab_line_not_found.
        "No pasa NAda
    ENDTRY.
    zcl_debug_log=>atrv_text = |  { bloque_nombre0 }{ bloque_tipo0  } called from  { bloque_nombre1 }{ bloque_tipo1 } |.
    zcl_debug_log=>debug_mosrtar_texto( ).
  ENDMETHOD.
ENDCLASS.
