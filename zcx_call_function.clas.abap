class ZCX_CALL_FUNCTION definition
  public
  inheriting from CX_STATIC_CHECK
  create public .

public section.

  constants ZCX_CALL_FUNCTION type SOTR_CONC value '5CD998BE73081EE8B4B479E2325F1D24'. "#EC NOTEXT
  data FUNCTION_NAME type MSGV1 .
  data SY_SUBRC type SY-SUBRC .

  methods CONSTRUCTOR
    importing
      !TEXTID like TEXTID optional
      !PREVIOUS like PREVIOUS optional
      !FUNCTION_NAME type MSGV1 optional
      !SY_SUBRC type SY-SUBRC optional .
protected section.
private section.
ENDCLASS.



CLASS ZCX_CALL_FUNCTION IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
TEXTID = TEXTID
PREVIOUS = PREVIOUS
.
 IF textid IS INITIAL.
   me->textid = ZCX_CALL_FUNCTION .
 ENDIF.
me->FUNCTION_NAME = FUNCTION_NAME .
me->SY_SUBRC = SY_SUBRC .
  endmethod.
ENDCLASS.
