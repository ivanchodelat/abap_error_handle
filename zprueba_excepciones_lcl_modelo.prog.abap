CLASS lcl_modelo DEFINITION.
  PUBLIC SECTION.
    METHODS grabar RAISING cx_sy_open_sql_db.
ENDCLASS.


CLASS lcl_modelo IMPLEMENTATION.
  METHOD grabar.
*RAISING cx_sy_open_sql_db.
   DATA wa_zwm_matbin TYPE zwm_matbin.
    DATA lit_zwm_matbin TYPE STANDARD TABLE OF zwm_matbin.
    wa_zwm_matbin-ernam = sy-uname.
    wa_zwm_matbin-ersda = sy-datlo.
    wa_zwm_matbin-lgnum = '50'.
    wa_zwm_matbin-LGTYP = '120'.
    wa_zwm_matbin-LGPLA = '12-01-02'.
    APPEND  wa_zwm_matbin TO lit_zwm_matbin.
    INSERT zwm_matbin FROM TABLE lit_zwm_matbin.
  ENDMETHOD.

ENDCLASS.
