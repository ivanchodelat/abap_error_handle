*"* use this source file for any macro definitions you need
*"* in the implementation part of the class
* Macro Read parameter
  DEFINE _macro_read_p_stvarv_debug.
    SELECT SINGLE low FROM tvarvc
              INTO &1
              WHERE name = &2
              AND type = 'P'.
    IF sy-subrc NE 0.
      " No dice nada
      "MESSAGE |{ lv_mensaje } { &2 }| TYPE 'W'.
      &1 = abap_false.
    ENDIF.
  END-OF-DEFINITION.